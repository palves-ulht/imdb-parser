# IMDB Parser #

A simple application to convert IMDB dataset files (http://www.imdb.com/interfaces) into CSV files (encoded in UTF-8).

Right now, it only supports movies and genres.