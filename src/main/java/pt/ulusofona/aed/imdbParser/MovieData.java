package pt.ulusofona.aed.imdbParser;

/**
 * Created by pedroalves on 01/03/17.
 */
public class MovieData {

    private String title;
    private int year;

    public MovieData(String title, int year) {
        this.title = title;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;

        if (o instanceof GenreData) {
            GenreData g = (GenreData) o;
            return title.equals(g.getTitle());
        }

        MovieData movieData = (MovieData) o;
        return title != null ? title.equals(movieData.title) : movieData.title == null;
    }

    @Override
    public int hashCode() {
        return title != null ? title.hashCode() : 0;
    }
}
