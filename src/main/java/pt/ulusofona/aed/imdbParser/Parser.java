package pt.ulusofona.aed.imdbParser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

/**
 * Created by pedroalves on 01/03/17.
 */
public class Parser {

    private static final String MOVIES_MARKER = "MOVIES LIST";
    private static final int MOVIES_SKIPS = 2;
    private static final String GENRES_MARKER = "8: THE GENRES LIST";
    private static final int GENRES_SKIPS = 2;
    private static final String ACTRESSES_MARKER = "THE ACTRESSES LIST";
    private static final int ACTRESS_SKIPS = 4;
    private static final String ACTOR_MARKER = "THE ACTORS LIST";
    private static final int ACTOR_SKIPS = 4;

    public static List<MovieData> parseMovies(String path) throws IOException {

        List<MovieData> movies = new ArrayList<>();

        BufferedReader fileReader = getFileReader(path, MOVIES_MARKER, MOVIES_SKIPS);

        String line = fileReader.readLine();
        while (line != null) {
            // get rid of blank lines and TV shows
            if ("".equals(line) || line.indexOf("(TV)") != -1) {
                line = fileReader.readLine();
                continue;
            }

            byte[] isoBytes = line.getBytes();
            String utfLine = new String(isoBytes, "UTF-8");
            String[] tuple = parseTuple(utfLine);
            if (tuple != null) {
                if (tuple[1].length() > 4) {
                    tuple[1] = tuple[1].substring(0, 4);
                }
                movies.add(new MovieData(tuple[0], Integer.parseInt(tuple[1])));
            }

            line = fileReader.readLine();
        }

        return movies;
    }

    public static List<GenreData> parseGenres(String path) throws IOException {

        List<GenreData> genres = new ArrayList<>();

        BufferedReader fileReader = getFileReader(path, GENRES_MARKER, GENRES_SKIPS);

        String line = fileReader.readLine();
        while (line != null) {

//             get rid of blank lines and TV shows
            if ("".equals(line) || line.contains("(TV)") || line.contains("????")) {
                line = fileReader.readLine();
                continue;
            }

            byte[] isoBytes = line.getBytes();
            String utfLine = new String(isoBytes, "UTF-8");
            String[] tuple = parseTuple(utfLine);
            if (tuple != null) {
                genres.add(new GenreData(tuple[0], tuple[1]));
            }

            line = fileReader.readLine();
        }


        return genres;
    }

    private static String[] parseTuple(String utfLine) {

        String[] tuple = new String[2];
        final int yearSep = utfLine.indexOf('\t');
        if (yearSep > 0) {
            tuple[0] = utfLine.substring(0, yearSep).trim();  // title
            tuple[0] = tuple[0].replace("\"","");
            tuple[1] = utfLine.substring(yearSep).trim();  // second element
            if (tuple[1].length() == 0 || tuple[1].charAt(0) == '?' || tuple[0].contains("{")) {
                return null;
            }
            return tuple;
        }
        return null;
    }

    private static BufferedReader getFileReader(final String file, String pattern,
                                                int skipLines) throws IOException, FileNotFoundException {
        BufferedReader fileReader;
        // support compressed files
        if (file.endsWith(".gz")) {
            fileReader = new BufferedReader(new InputStreamReader(
                    new GZIPInputStream(new FileInputStream(file)), "ISO-8859-1"));
        } else if (file.endsWith(".zip")) {
            fileReader = new BufferedReader(new InputStreamReader(
                    new ZipInputStream(new FileInputStream(file)), "ISO-8859-1"));
        } else {
            fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));
        }

        String line = "";
        while (!pattern.equals(line)) {
            line = fileReader.readLine();
        }
        for (int i = 0; i < skipLines; i++) {
            line = fileReader.readLine();
        }

        return fileReader;
    }
}
