package pt.ulusofona.aed.imdbParser;

import java.util.List;

/**
 * Created by pedroalves on 01/03/17.
 */
public class App {

    public static void main(String[] args) throws Exception {

        List<MovieData> movies = Parser.parseMovies("../imdb-dataset/movies.list.gz");
        CsvWriter.writeMovies(movies, "../imdb-dataset/movies.csv");

        List<GenreData> genres = Parser.parseGenres("../imdb-dataset/genres.list.gz");
        CsvWriter.writeGenres(genres, "../imdb-dataset/genres.csv");

    }
}
