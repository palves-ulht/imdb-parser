package pt.ulusofona.aed.imdbParser;

/**
 * Created by pedroalves on 01/03/17.
 */
public class GenreData {

    private String title;
    private String genre;

    public GenreData(String title, String genre) {
        this.title = title;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;

        if (o instanceof MovieData) {
            MovieData m = (MovieData) o;
            return title.equals(m.getTitle());
        }

        GenreData genreData = (GenreData) o;
        return title != null ? title.equals(genreData.title) : genreData.title == null;
    }

    @Override
    public int hashCode() {
        return title != null ? title.hashCode() : 0;
    }
}
