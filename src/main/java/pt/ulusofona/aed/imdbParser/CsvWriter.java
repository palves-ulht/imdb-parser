package pt.ulusofona.aed.imdbParser;

import java.io.*;
import java.util.List;

/**
 * Created by pedroalves on 01/03/17.
 */
public class CsvWriter {

    public static void writeMovies(List<MovieData> movies, String path) throws Exception {
        PrintWriter pWriter = null;
        try {
            pWriter = new PrintWriter(new FileWriter(path));

            for (MovieData m : movies) {
                pWriter.println(m.getTitle() + ";" + m.getYear());
            }
        } finally {
            if (pWriter != null) {
                pWriter.close();
            }
        }
    }

    public static void writeGenres(List<GenreData> genres, String path) throws Exception {
        PrintWriter pWriter = null;
        try {
            pWriter = new PrintWriter(new FileWriter(path));

            for (GenreData g : genres) {
                pWriter.println(g.getTitle() + ";" + g.getGenre());
            }
        } finally {
            if (pWriter != null) {
                pWriter.close();
            }
        }
    }
}
